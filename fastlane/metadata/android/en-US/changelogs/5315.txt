## v1.8.1.3, build 5315
New 
- Nothing

Fixed
- Fixed bug where Weight edit wouldn't load the comment
- Setting/Changing the filter is now reflected in the data tabs right away.

Known issues
- None
