## v1.7.5, build 5308
New

- Added Database encryption 
- Added feature to activate first edit field whenever an edit screen is opened, saves one click for each entry :-)
- Added "Show Pulse" setting to further clean up Blood Pressure chart
- Added database size to About screen
- Added number of items exported to backup success messages
- Added security section to About dialog

Fixed

- Brought back changelog.md, will drop adding notes to version tags from now on
- Fixed race condition during import which may have led to partial imports with large imports (> 1000 rows)
- Fixed calculating max and min values to properly place the charts in the middle of the screen
- Removed "item saved" messages after an item got added, looked like an error and didn't serve a purpose because the new value is visible at the top of the list anyway.
- Enlarged the blood pressure quickedit fields somewhat to make the hint message fit in german language
- Made sure chart origins starts on a 10er number instead of uneven numbers
