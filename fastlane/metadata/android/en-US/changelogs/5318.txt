## v1.9.0.2, build 5318
New 

- Nothing

Fixed

- Fixed a nasty bug in water tab which would crash the app if no data was compiled for today

Known issues

- Water Intake report not (yet) showing summary
- Diary PDF report not showing state
