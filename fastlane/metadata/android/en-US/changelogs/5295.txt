New/improved
- Bumped up version number to 1.7
- Added date filter capability
- Improved usability when handling authentication errors
- Added disclaimer dialog for first launch
- Moved data import to separate thread to cater for large imports
- Large scale testing. 10k records per category = 27 years worth of one entry per day

Fixes
- Weight value was not showing when highlighting values was off
- Fixed a racing condition where clearing the db before a data import would lead to unpredictable results