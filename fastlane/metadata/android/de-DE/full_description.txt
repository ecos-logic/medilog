Datenschutzfreundliches Festhalten von Blutdruck, Gewicht, und täglicher Trinkmenge inklusive einfacher Diagramme und der Möglichkeit die vertraulichen Daten sicher weiterzuleiten.  
 
 * Unterstützt Biometrie (Fingerabdruck) zur Anmeldung 
 * Diagramme mit Trends und Schwellwerten
 * CSV und PDF Export in passwortgeschützter ZIP Datei oder Klartext CSV Datei
 
