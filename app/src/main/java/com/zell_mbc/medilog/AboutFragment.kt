package com.zell_mbc.medilog

import android.os.Bundle
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.bloodpressure.BloodPressureViewModel
import com.zell_mbc.medilog.diary.DiaryViewModel
import com.zell_mbc.medilog.water.WaterViewModel
import com.zell_mbc.medilog.weight.WeightViewModel
import kotlinx.android.synthetic.main.activity_about.*

class AboutFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val colourStyle = sharedPref.getString(SettingsActivity.KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))

        // Change application icon
        if (colourStyle == this.getString(R.string.green)) {
            ivLogo.setImageResource(R.mipmap.ic_launcher_green)
        }
        if (colourStyle == this.getString(R.string.blue)) {
            ivLogo.setImageResource(R.mipmap.ic_launcher_blue)
        }
        if (colourStyle == this.getString(R.string.red)) {
            ivLogo.setImageResource(R.mipmap.ic_launcher_red)
        }
        val versionCode = "Build " + BuildConfig.VERSION_CODE
        val versionName = "Version " + BuildConfig.VERSION_NAME

        tvVersion.text = versionName
        tvBuild.text = versionCode

        Linkify.addLinks(tvEmail, Linkify.ALL)
        Linkify.addLinks(tvMediLogLicenseUrl, Linkify.ALL)
        Linkify.addLinks(tvAndroidPlotUrl, Linkify.ALL)
        Linkify.addLinks(tvMediLogUrl, Linkify.ALL)
        Linkify.addLinks(tvPreferenceFixUrl, Linkify.ALL)
        Linkify.addLinks(tvZip4j, Linkify.ALL)

        // Statistics
        val c = context
        if (c == null) {
            Log.d("--------------- Debug", " Chart Empty Context")
            return
        }
        val showBloodPressureTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showBloodPressureTab, true)
        val showWeightTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWeightTab, true)
        val showDiaryTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showDiaryTab, true)
        val showWaterTab = sharedPref.getBoolean(SettingsActivity.KEY_PREF_showWaterTab, true)

        if (sharedPref.getBoolean("encryptedDB", false)) tvEncryptedDB.text = getString(R.string.databaseEncryptionOn)
        else  tvEncryptedDB.text = getString(R.string.databaseEncryptionOff)

        if (sharedPref.getBoolean(SettingsActivity.KEY_PREF_BIOMETRIC, false)) tvAccessProtection.text = getString(R.string.biometricProtectionOn)
        else  tvAccessProtection.text = getString(R.string.biometricProtectionOff)

        val ss = sharedPref.getString(SettingsActivity.KEY_PREF_PASSWORD,"")
        if (ss != null) {
            if (ss.isNotEmpty()) tvBackupProtection.text = getString(R.string.protectedBackupOn)
            else  tvBackupProtection.text = getString(R.string.protectedBackupOff)
        }

        var s: String
        if (showWeightTab) {
            val weightUnit = sharedPref.getString(SettingsActivity.KEY_PREF_WEIGHTUNIT, "kg")
            val weightViewModel = ViewModelProvider(this).get(WeightViewModel::class.java)
            weightViewModel.init()
            s = getString(R.string.weight) + ": " + weightViewModel.getSize(false) + " " + getString(R.string.entries) + ", min " + weightViewModel.getMin() + " " + weightUnit + ", max " + weightViewModel.getMax() + " " + weightUnit
            tvWeightStats.text = s
        }
        if (showBloodPressureTab) {
            val bloodPressureViewModel = ViewModelProvider(this).get(BloodPressureViewModel::class.java)
            bloodPressureViewModel.init()
            s = getString(R.string.bloodPressure) + ": " + bloodPressureViewModel.getSize(false) + " " + getString(R.string.entries) + ", " + " " + getString(R.string.systolic) + " min " + bloodPressureViewModel.getMinSys() + ", max " + bloodPressureViewModel.getMaxSys()
            tvBloodPressureStats.text = s
            s = " " + getString(R.string.diastolic) +" min " + bloodPressureViewModel.getMinDia() + ", max " + bloodPressureViewModel.getMaxDia()
            tvBloodPressureStats2.text = s
        }
        if (showDiaryTab) {
            val diaryViewModel = ViewModelProvider(this).get(DiaryViewModel::class.java)
            diaryViewModel.init()
            s = getString(R.string.diary) + ": " + diaryViewModel.getSize(false) + " " + " " + getString(R.string.entries)
            tvDiaryStats.text = s
        }
        if (showWaterTab) {
            val waterUnit = sharedPref.getString(SettingsActivity.KEY_PREF_WATERUNIT, "ml")
            val waterViewModel = ViewModelProvider(this).get(WaterViewModel::class.java)
            waterViewModel.init()
            s = getString(R.string.water) + ": " + waterViewModel.getSize(false) + " " + getString(R.string.entries) + ", min " + waterViewModel.getMin() + " " + waterUnit + ", max " + waterViewModel.getMax() + " " + waterUnit
            tvWaterStats.text = s
        }

        val dbName = "MediLogDatabase"
        val f = context?.getDatabasePath(dbName)
        val dbSize = f?.length()
        if (dbSize != null) {
            s = getString(R.string.databaseSize) + " " + (dbSize/1024).toString() + " kb"
            tvDbSize.text = s
        }
    }

}