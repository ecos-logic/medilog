package com.zell_mbc.medilog

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import com.zell_mbc.medilog.bloodpressure.BloodPressureHelper

class SettingsActivity : AppCompatActivity(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

        override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
            // Instantiate the new Fragment
            val args = pref.extras
            val fragment = supportFragmentManager.fragmentFactory.instantiate(
                    classLoader,
                    pref.fragment)
            fragment.arguments = args
            fragment.setTargetFragment(caller, 0)
            // Replace the existing Fragment with the new Fragment
//            supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, fragment)
                    .addToBackStack(null)
                    .commit()
            return true
        }

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        when (sharedPref.getString(KEY_PREF_COLOUR_STYLE, this.getString(R.string.blue))) {
            this.getString(R.string.green) -> theme.applyStyle(R.style.AppThemeGreenBar, true)
            this.getString(R.string.blue)  -> theme.applyStyle(R.style.AppThemeBlueBar, true)
            this.getString(R.string.red)   -> theme.applyStyle(R.style.AppThemeRedBar, true)
        }
        supportFragmentManager.beginTransaction().replace(android.R.id.content, SettingsFragment()).commit()
    }

    public override fun onStop() {
        super.onStop()
        // Check validity of bp values
        BloodPressureHelper(this)
    }

    companion object {
        const val KEY_PREF_DELIMITER = "delimiter"

        const val KEY_PREF_WEIGHTUNIT         = "etWeightUnit"
        const val KEY_PREF_weightThreshold    = "etWeightThreshold"
        const val KEY_PREF_showWeightThreshold = "cbShowWeightThreshold"
        const val KEY_PREF_showWeightGrid     = "cbShowWeightGrid"
        const val KEY_PREF_showWeightLegend   = "cbShowWeightLegend"
        const val KEY_PREF_WEIGHT_LINEAR_TRENDLINE = "cbWeightLinearTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_TRENDLINE = "cbWeightMovingAverageTrendline"
        const val KEY_PREF_WEIGHT_MOVING_AVERAGE_SIZE = "etWeightMovingAverageSize"
        const val KEY_PREF_weightDayStepping = "cbWeightDayStepping"
        const val KEY_PREF_weightBarChart = "cbWeightBarChart"

        const val KEY_PREF_waterThreshold     = "evWaterThreshold"
        const val KEY_PREF_WATERUNIT          = "evWaterUnit"
        const val KEY_PREF_SUMMARYPDF         = "swShowSummaryinPDF"
//        const val KEY_PREF_waterDayStepping   = "checkBox_waterDayStepping"
        const val KEY_PREF_waterBarChart      = "cbWaterBarChart"
        const val KEY_PREF_showWaterThreshold = "cbShowWaterThreshold"
        const val KEY_PREF_showWaterGrid      = "cbShowWaterGrid"
        const val KEY_PREF_showWaterLegend    = "cbShowWaterLegend"

        const val KEY_PREF_showBloodPressureThreshold = "cbShowBloodPressureThreshold"
        const val KEY_PREF_showBloodPressureGrid      = "cbShowBloodPressureGrid"
        const val KEY_PREF_showBloodPressureLegend    = "cbShowBloodPressureLegend"
        const val KEY_PREF_bpBarChart                 = "cbBloodPressureBarChart"
        const val KEY_PREF_bpDayStepping = "cbBloodPressureDayStepping"
        const val KEY_PREF_SHOWPULSE = "cbShowPulse"
        const val KEY_PREF_hyperGrade3 = "grade3"
        const val KEY_PREF_hyperGrade2 = "grade2"
        const val KEY_PREF_hyperGrade1 = "grade1"
        const val KEY_PREF_BLOODPRESSURE_LINEAR_TRENDLINE = "cbBloodPressureLinearTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_TRENDLINE = "cbBloodPressureMovingAverageTrendline"
        const val KEY_PREF_BLOODPRESSURE_MOVING_AVERAGE_SIZE = "etBloodPressureMovingAverageSize"

        const val KEY_PREF_showWeightTab        = "showWeightTab"
        const val KEY_PREF_showWaterTab         = "cbShowWaterTab"
        const val KEY_PREF_showBloodPressureTab = "showBloodPressureTab"
        const val KEY_PREF_showDiaryTab         = "showDiaryTab"
        const val KEY_PREF_SHOWTABICON          = "showTabIcon"
        const val KEY_PREF_SHOWTABTEXT          = "showTabText"
        const val KEY_PREF_SCROLLABLETABS       = "swScrollableTabs"
        const val KEY_PREF_TABTRANSITIONS       = "liTabTransitions"

        const val KEY_PREF_COLOUR = "cbSetColour"
        const val KEY_PREF_TEXT_SIZE = "listTextSize"
        const val KEY_PREF_USER = "userName"
        const val KEY_PREF_ZIPBACKUP = "zipBackup"
        const val KEY_PREF_COLOUR_STYLE = "colourStyle"
        const val KEY_PREF_PASSWORD = "zipPassword"
        const val KEY_PREF_BIOMETRIC = "enableBiometric"
        const val KEY_PREF_QUICKENTRY = "quickEntry"
    }
}