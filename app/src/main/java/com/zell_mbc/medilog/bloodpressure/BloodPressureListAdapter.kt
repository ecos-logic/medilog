package com.zell_mbc.medilog.bloodpressure

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import com.zell_mbc.medilog.*
import java.text.DateFormat

class BloodPressureListAdapter internal constructor(context: Context) : RecyclerView.Adapter<BloodPressureListAdapter.BloodPressureViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var clickListener: ItemClickListener? = null

    private var items = emptyList<BloodPressure>()
    private val textSize: Float
    private var dateFormat: DateFormat
    private var timeFormat: DateFormat
    private val highlightValues: Boolean
    private var bpHelper: BloodPressureHelper

    inner class BloodPressureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val ivHeartRhythm: ImageView = itemView.findViewById(R.id.ivHeartRhythmIssue)
        val tvDate: TextView = itemView.findViewById(R.id.tvBloodPressureDate)
        val tvSys: TextView = itemView.findViewById(R.id.tvBloodPressureSys)
        val tvDia: TextView = itemView.findViewById(R.id.tvBloodPressureDia)
        val tvPulse: TextView = itemView.findViewById(R.id.tvBloodPressurePulse)
        val tvComment: TextView = itemView.findViewById(R.id.tvBloodPressureComment)

        override fun onClick(view: View) {
            if (clickListener != null) clickListener!!.onItemClick(view, adapterPosition)
        }

        init {
            tvDate.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvSys.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvDia.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvPulse.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            tvComment.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSize)
            itemView.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BloodPressureListAdapter.BloodPressureViewHolder {
        val itemView = inflater.inflate(R.layout.bloodpressureview_row, parent, false)
        return BloodPressureViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BloodPressureListAdapter.BloodPressureViewHolder, position: Int) {
        val current = items[position]

        if (current.state > 0) holder.ivHeartRhythm.visibility = View.VISIBLE
        else holder.ivHeartRhythm.visibility = View.INVISIBLE
        val tmpString = dateFormat.format(current.timestamp) + " - " + timeFormat.format(current.timestamp)
        holder.tvDate.text = tmpString

        if (highlightValues) {
            val orgColour = holder.tvDate.textColors.defaultColor
            when (bpHelper.sysGrade(current.sys)) {
                bpHelper.hyperGrade3 -> {
                    holder.tvSys.setTextColor(Color.rgb(255, 0, 0))
                    holder.tvSys.setTypeface(null, Typeface.BOLD)
                }
                bpHelper.hyperGrade2 -> holder.tvSys.setTextColor(Color.rgb(255, 0, 0))
                bpHelper.hyperGrade1 -> holder.tvSys.setTextColor(Color.rgb(255, 165, 0))
                else ->                 holder.tvSys.setTextColor(orgColour)

            }
            holder.tvSys.text = current.sys.toString()

            when (bpHelper.diaGrade(current.dia)) {
                bpHelper.hyperGrade3 -> {
                    holder.tvDia.setTextColor(Color.rgb(255, 0, 0))
                    holder.tvDia.setTypeface(null, Typeface.BOLD)
                }
                bpHelper.hyperGrade2 -> holder.tvDia.setTextColor(Color.rgb(255, 0, 0))
                bpHelper.hyperGrade1 -> holder.tvDia.setTextColor(Color.rgb(255, 165, 0))
                else ->                 holder.tvDia.setTextColor(orgColour)
            }
            holder.tvDia.text = current.dia.toString()
            }
        else {
            holder.tvSys.text = current.sys.toString()
            holder.tvDia.text = current.dia.toString()
            }
        holder.tvPulse.text = current.pulse.toString()
        holder.tvComment.text = current.comment
    }

    internal fun setItems(bloodPressures: List<BloodPressure>) {
        this.items = bloodPressures
        notifyDataSetChanged()
    }

    fun getItemAt(position: Int): BloodPressure {
        return items[position]
    }

    // allows clicks events to be caught
    fun setClickListener(itemClickListener: ItemClickListener?) {
        clickListener = itemClickListener
    }


    // parent activity will implement this method to respond to click events
    interface ItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    override fun getItemCount() = items.size

    init {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        dateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
        timeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)
        textSize = java.lang.Float.valueOf(sharedPref.getString(SettingsActivity.KEY_PREF_TEXT_SIZE, "15")!!)
        highlightValues = sharedPref.getBoolean(SettingsActivity.KEY_PREF_COLOUR, false)
        bpHelper = BloodPressureHelper(context)
    }
}