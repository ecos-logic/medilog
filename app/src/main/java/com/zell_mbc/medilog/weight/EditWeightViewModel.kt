package com.zell_mbc.medilog.weight

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.MediLogDB
import com.zell_mbc.medilog.Weight
import com.zell_mbc.medilog.base.EditDataViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class EditWeightViewModel(application: Application): EditDataViewModel(application) {
        override lateinit var repository: WeightRepository
        lateinit var dao: WeightDao

        fun init() {
            dao = MediLogDB.getDatabase(getApplication()).weightDao()
            repository = WeightRepository(dao, 0L, 0L)
        }

        override fun getItem(id: Int): Weight? {
            var item: Weight? = null
            runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
                item = repository.getItem(id) }
                j.join() }
            return item
        }

        fun update(weight: Weight) = viewModelScope.launch(Dispatchers.IO) {
            repository.update(weight)
        }
}