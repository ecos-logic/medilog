package com.zell_mbc.medilog.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.DataItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

abstract class EditDataViewModel(application: Application): AndroidViewModel(application) {
    private val tmpComment = "temporaryEntry-YouShouldNeverSeeThis"
    abstract val repository: DataRepository

    abstract fun getItem(id: Int): DataItem?

    // Used by editItem Fragments to delete leftover temporary entries
    fun delete() = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(tmpComment)
    }

    fun isTmpItem(comment: String?): Boolean { return (comment.equals(tmpComment))    }
}