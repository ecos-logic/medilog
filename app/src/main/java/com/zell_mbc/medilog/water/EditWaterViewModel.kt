package com.zell_mbc.medilog.water

import android.app.Application
import androidx.lifecycle.viewModelScope
import com.zell_mbc.medilog.MediLogDB
import com.zell_mbc.medilog.Water
import com.zell_mbc.medilog.base.EditDataViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class EditWaterViewModel(application: Application): EditDataViewModel(application) {
    override lateinit var repository: WaterRepository
    lateinit var dao: WaterDao

    fun init() {
        dao = MediLogDB.getDatabase(getApplication()).waterDao()
        repository = WaterRepository(dao, 0L, 0L)
    }

    override fun getItem(id: Int): Water? {
        var item: Water? = null
        runBlocking { val j = viewModelScope.launch(Dispatchers.IO) {
            item = repository.getItem(id) }
            j.join() }
        return item
    }

    fun update(water: Water) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(water)
    }
}